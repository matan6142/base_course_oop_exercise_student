package Entities;

public enum FlightStatus {
    ABLE_TO_FLIGHT,
    UNABLE_TO_FLIGHT,
    IN_AIR

}
