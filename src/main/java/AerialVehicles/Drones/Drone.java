package AerialVehicles.Drones;

import AerialVehicles.AerialVehicle;
import AerialVehicles.Exception.UnableToFlightException;
import Entities.Coordinates;
import Entities.FlightStatus;

public abstract class Drone extends AerialVehicle {

    public Drone(double hoursBeforeRepair, Coordinates homeBase) {
        super(hoursBeforeRepair, homeBase);
    }

    public String hoverOverLocation(Coordinates destination) throws UnableToFlightException {
        String hoverResult = null;

        switch (this.getStatus()) {
            case ABLE_TO_FLIGHT: {
                this.setStatus(FlightStatus.IN_AIR);
                hoverResult = this.performHovering(destination);

                break;
            }
            case IN_AIR: {
                hoverResult = this.performHovering(destination);

                break;
            }
            case UNABLE_TO_FLIGHT: {
                throw new UnableToFlightException("The drone is unable to flight");
            }
        }

        return hoverResult;
    }

    /**
     *
     * @param destination upon where to hover
     * @return the status of perform hover
     */
    private String performHovering(Coordinates destination) {
        String message = String.format("Hovering over: %f, %f", destination.getLatitude(), destination.getLongitude());
        System.out.println(message);
        return (message);
    }
}
