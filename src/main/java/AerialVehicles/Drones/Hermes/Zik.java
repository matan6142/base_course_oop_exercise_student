package AerialVehicles.Drones.Hermes;


import AerialVehicles.AdditionTypes.CameraTypes;
import AerialVehicles.AdditionTypes.MissileTypes;
import AerialVehicles.AdditionTypes.SensorTypes;
import AerialVehicles.VehicleTypes.AerialBdaVehicle;
import AerialVehicles.VehicleTypes.AerialIntelligenceVehicle;
import Entities.Coordinates;

public class Zik extends HermesDrone implements AerialIntelligenceVehicle, AerialBdaVehicle {
    private CameraTypes cameraType;
    private SensorTypes sensorType;

    public Zik(Coordinates homeBase, CameraTypes cameraType, SensorTypes sensorType) {
        super(homeBase);
        this.cameraType = cameraType;
        this.sensorType = sensorType;
    }

    @Override
    public CameraTypes getCameraType() {
        return cameraType;
    }

    @Override
    public void setCameraType(CameraTypes cameraType) {
        this.cameraType = cameraType;
    }

    @Override
    public SensorTypes getSensorType() {
        return sensorType;
    }

    @Override
    public void setSensorType(SensorTypes sensorType) {
        this.sensorType = sensorType;
    }
}