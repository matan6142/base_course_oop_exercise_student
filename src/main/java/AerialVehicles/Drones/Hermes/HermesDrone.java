package AerialVehicles.Drones.Hermes;

import AerialVehicles.Drones.Drone;
import Entities.Coordinates;

public abstract class HermesDrone extends Drone {
    private static final double hoursBeforeRepair = 100;
    public HermesDrone(Coordinates homeBase) {
        super(HermesDrone.hoursBeforeRepair, homeBase);
    }
}
