package AerialVehicles.Drones.Harmon;

import AerialVehicles.Drones.Drone;
import Entities.Coordinates;

public abstract class HaronDrone extends Drone {
    private static final double hoursBeforeRepair = 150;

    public HaronDrone(Coordinates homeBase) {
        super(hoursBeforeRepair, homeBase);
    }
}
