package AerialVehicles.Drones.Harmon;

import AerialVehicles.AdditionTypes.MissileTypes;
import AerialVehicles.AdditionTypes.SensorTypes;
import AerialVehicles.VehicleTypes.AerialAttackVehicle;
import AerialVehicles.VehicleTypes.AerialIntelligenceVehicle;
import Entities.Coordinates;

public class Eitan extends HaronDrone implements AerialIntelligenceVehicle, AerialAttackVehicle {
    private int missileAmount;
    private MissileTypes missileType;
    private SensorTypes sensorType;

    public Eitan(Coordinates homeBase, int missileAmount, MissileTypes missileType, SensorTypes sensorType) {
        super(homeBase);
        this.missileAmount = missileAmount;
        this.missileType = missileType;
        this.sensorType = sensorType;
    }

    @Override
    public int getMissileAmount() {
        return missileAmount;
    }

    @Override
    public void setMissileAmount(int missileAmount) {
        this.missileAmount = missileAmount;
    }

    @Override
    public MissileTypes getMissileType() {
        return missileType;
    }

    @Override
    public void setMissileType(MissileTypes missileType) {
        this.missileType = missileType;
    }

    @Override
    public SensorTypes getSensorType() {
        return sensorType;
    }

    @Override
    public void setSensorType(SensorTypes sensorType) {
        this.sensorType = sensorType;
    }
}
