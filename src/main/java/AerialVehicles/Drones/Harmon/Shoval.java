package AerialVehicles.Drones.Harmon;


import AerialVehicles.AdditionTypes.CameraTypes;
import AerialVehicles.AdditionTypes.MissileTypes;
import AerialVehicles.AdditionTypes.SensorTypes;
import AerialVehicles.VehicleTypes.AerialAttackVehicle;
import AerialVehicles.VehicleTypes.AerialBdaVehicle;
import AerialVehicles.VehicleTypes.AerialIntelligenceVehicle;
import Entities.Coordinates;

public class Shoval extends HaronDrone implements AerialIntelligenceVehicle, AerialBdaVehicle, AerialAttackVehicle {
    private int missileAmount;
    private MissileTypes missileType;
    private CameraTypes cameraType;
    private SensorTypes sensorType;

    public Shoval(Coordinates homeBase, int missileAmount, MissileTypes missileType, CameraTypes cameraType, SensorTypes sensorType) {
        super(homeBase);
        this.missileAmount = missileAmount;
        this.missileType = missileType;
        this.cameraType = cameraType;
        this.sensorType = sensorType;
    }

    @Override
    public int getMissileAmount() {
        return missileAmount;
    }

    @Override
    public void setMissileAmount(int missileAmount) {
        this.missileAmount = missileAmount;
    }

    @Override
    public MissileTypes getMissileType() {
        return missileType;
    }

    @Override
    public void setMissileType(MissileTypes missileType) {
        this.missileType = missileType;
    }

    @Override
    public CameraTypes getCameraType() {
        return cameraType;
    }

    @Override
    public void setCameraType(CameraTypes cameraType) {
        this.cameraType = cameraType;
    }

    @Override
    public SensorTypes getSensorType() {
        return sensorType;
    }

    @Override
    public void setSensorType(SensorTypes sensorType) {
        this.sensorType = sensorType;
    }
}

