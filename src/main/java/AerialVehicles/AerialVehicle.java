package AerialVehicles;


import AerialVehicles.Exception.UnableToFlightException;
import Entities.Coordinates;
import Entities.FlightStatus;

public abstract class AerialVehicle {
    private FlightStatus status;
    private double hoursInAirSinceRepair;
    private Coordinates homeBase;
    private double hoursBeforeRepair;

    public AerialVehicle(double hoursBeforeRepair, Coordinates homeBase) {
        // New Aerial Vehicle so every part is new
        this.setStatus(FlightStatus.ABLE_TO_FLIGHT);
        this.setHoursInAirSinceRepair(0);
        this.setHoursBeforeRepair(hoursBeforeRepair);
        this.setHomeBase(homeBase);
    }

    public void flyTo(Coordinates destination) throws UnableToFlightException {
        switch (this.getStatus()) {
            case ABLE_TO_FLIGHT: {
                this.setStatus(FlightStatus.IN_AIR);
                this.performFlyingTo(destination);

                break;
            }
            case IN_AIR: {
                this.performFlyingTo(destination);

                break;
            }
            case UNABLE_TO_FLIGHT: {
                System.out.println("Aerial Vehicle isn't ready to fly");
                throw new UnableToFlightException("The aerial vehicle is unable to flight");
            }
        }

    }

    private void performFlyingTo(Coordinates destination) {
        System.out.println(String.format("Flying to: %f, %f", destination.getLatitude(), destination.getLongitude()));
    }

    private void performLandingOn(Coordinates destination) {
        System.out.println(String.format("Landing on: %f, %f", destination.getLatitude(), destination.getLongitude()));
    }

    public void land(Coordinates destination) {
        if (this.status.equals(FlightStatus.IN_AIR)) {
            performLandingOn(destination);
            this.check();
        }
    }

    public void check() {
        if (this.hoursInAirSinceRepair >= this.hoursBeforeRepair) {
            this.setStatus(FlightStatus.UNABLE_TO_FLIGHT);
            this.repair();
        } else {
            this.setStatus(FlightStatus.ABLE_TO_FLIGHT);
        }
    }

    public void repair() {
        this.hoursInAirSinceRepair = 0;
        this.setStatus(FlightStatus.ABLE_TO_FLIGHT);
    }

    public FlightStatus getStatus() {
        return status;
    }

    protected void setStatus(FlightStatus newStatus) {
        this.status = newStatus;
    }

    public void setHoursInAirSinceRepair(double hoursInAirSinceRepair) {
        this.hoursInAirSinceRepair = hoursInAirSinceRepair;
    }

    public void setHoursBeforeRepair(double hoursBeforeRepair) {
        this.hoursBeforeRepair = hoursBeforeRepair;
    }

    public Coordinates getHomeBase() {
        return homeBase;
    }

    public void setHomeBase(Coordinates homeBase) {
        this.homeBase = homeBase;
    }
}
