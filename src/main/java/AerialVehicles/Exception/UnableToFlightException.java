package AerialVehicles.Exception;

public class UnableToFlightException extends Exception{
    public UnableToFlightException(String message) {
        super(message);
    }
}
