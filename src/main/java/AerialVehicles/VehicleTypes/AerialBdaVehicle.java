package AerialVehicles.VehicleTypes;

import AerialVehicles.AdditionTypes.CameraTypes;

public interface AerialBdaVehicle {
    CameraTypes getCameraType();
    void setCameraType(CameraTypes cameraType);
}
