package AerialVehicles.VehicleTypes;

import AerialVehicles.AdditionTypes.MissileTypes;

public interface AerialAttackVehicle {
    int getMissileAmount();
    void setMissileAmount(int missileAmount);
    MissileTypes getMissileType();
    void setMissileType(MissileTypes missileType);
}
