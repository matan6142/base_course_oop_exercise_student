package AerialVehicles.VehicleTypes;

import AerialVehicles.AdditionTypes.SensorTypes;

public interface AerialIntelligenceVehicle {
    SensorTypes getSensorType();
    void setSensorType(SensorTypes sensorType);
}
