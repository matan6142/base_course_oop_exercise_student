package AerialVehicles.FighterJets;

import AerialVehicles.AdditionTypes.MissileTypes;
import AerialVehicles.AdditionTypes.SensorTypes;
import AerialVehicles.VehicleTypes.AerialAttackVehicle;
import AerialVehicles.VehicleTypes.AerialIntelligenceVehicle;
import Entities.Coordinates;

public class F15 extends FighterJets implements AerialAttackVehicle, AerialIntelligenceVehicle {
    private int missileAmount;
    private MissileTypes missileType;
    private SensorTypes sensorType;

    public F15(Coordinates homeBase, int missileAmount, MissileTypes missileType, SensorTypes sensorType) {
        super(homeBase);
        this.missileAmount = missileAmount;
        this.missileType = missileType;
        this.sensorType = sensorType;
    }

    @Override
    public int getMissileAmount() {
        return this.missileAmount;
    }

    @Override
    public void setMissileAmount(int missileAmount) {
        this.missileAmount = missileAmount;
    }

    @Override
    public MissileTypes getMissileType() {
        return missileType;
    }

    @Override
    public void setMissileType(MissileTypes missileType) {
        this.missileType = missileType;
    }

    @Override
    public SensorTypes getSensorType() {
        return sensorType;
    }

    @Override
    public void setSensorType(SensorTypes sensorType) {
        this.sensorType = sensorType;
    }
}
