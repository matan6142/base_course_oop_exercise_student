package AerialVehicles.FighterJets;

import AerialVehicles.AdditionTypes.CameraTypes;
import AerialVehicles.AdditionTypes.MissileTypes;
import AerialVehicles.VehicleTypes.AerialAttackVehicle;
import AerialVehicles.VehicleTypes.AerialBdaVehicle;
import Entities.Coordinates;

public class F16 extends FighterJets implements AerialAttackVehicle, AerialBdaVehicle {
    private int missileAmount;
    private MissileTypes missileType;
    private CameraTypes cameraType;

    public F16(Coordinates homeBase, int missileAmount, MissileTypes missileType, CameraTypes cameraType) {
        super(homeBase);
        this.missileAmount = missileAmount;
        this.missileType = missileType;
        this.cameraType = cameraType;
    }

    @Override
    public int getMissileAmount() {
        return missileAmount;
    }

    @Override
    public void setMissileAmount(int missileAmount) {
        this.missileAmount = missileAmount;
    }

    @Override
    public MissileTypes getMissileType() {
        return missileType;
    }

    @Override
    public void setMissileType(MissileTypes missileType) {
        this.missileType = missileType;
    }

    @Override
    public CameraTypes getCameraType() {
        return cameraType;
    }

    @Override
    public void setCameraType(CameraTypes cameraType) {
        this.cameraType = cameraType;
    }
}
