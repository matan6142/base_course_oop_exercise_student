package AerialVehicles.FighterJets;

import AerialVehicles.AerialVehicle;
import Entities.Coordinates;

public abstract class FighterJets extends AerialVehicle {
    private static final double hoursBeforeRepair = 250;
    public FighterJets(Coordinates homeBase) {
        super(FighterJets.hoursBeforeRepair, homeBase);
    }
}
