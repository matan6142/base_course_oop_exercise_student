package AerialVehicles.AdditionTypes;

public enum CameraTypes {
    REGULAR("Regular"),
    THERMAL("Thermal"),
    NIGHT_VISION("Night Vision");

    private String value;

    CameraTypes(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }
}
