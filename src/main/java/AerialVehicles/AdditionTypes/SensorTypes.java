package AerialVehicles.AdditionTypes;

public enum SensorTypes {
    INFRA_RED("Infra Red"),
    ELINT("Elint");

    private String value;

    SensorTypes(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }
}
