package AerialVehicles.AdditionTypes;

public enum MissileTypes {
    PYTHON("Python"),
    AMRAM("Amram"),
    SPICE250("Spice250");

    private String value;

    MissileTypes(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }
}
