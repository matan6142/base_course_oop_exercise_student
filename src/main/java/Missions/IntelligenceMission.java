package Missions;

import AerialVehicles.AerialVehicle;
import AerialVehicles.VehicleTypes.AerialIntelligenceVehicle;
import Entities.Coordinates;

public class IntelligenceMission extends Mission{
    private String region;

    public IntelligenceMission(Coordinates destination, String pilotName, AerialVehicle aerialVehicle, String region) {
        super(destination, pilotName, aerialVehicle);
        this.region = region;
    }

    @Override
    public String executeMission() throws AerialVehicleNotCompatibleException {
        try {
            return String.format("%s: %s Collecting Data in %s with: sensor type: %s",
                                this.pilotName,
                                this.aerialVehicle.getClass().getSimpleName(),
                                region,
                                ((AerialIntelligenceVehicle)this.aerialVehicle).getSensorType().getValue());
        } catch (ClassCastException e) {
            throw new AerialVehicleNotCompatibleException(String.format("%s Types are unable to do %s",
                                                            this.aerialVehicle.getClass().getSimpleName(),
                                                            this.getClass().getSimpleName()));
        }

    }
}
