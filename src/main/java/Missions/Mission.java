package Missions;

import AerialVehicles.AerialVehicle;
import AerialVehicles.Exception.UnableToFlightException;
import Entities.Coordinates;

public abstract class Mission{
    protected Coordinates destination;
    protected String pilotName;
    protected AerialVehicle aerialVehicle;

    public Mission(Coordinates destination, String pilotName, AerialVehicle aerialVehicle) {
        this.destination = destination;
        this.pilotName = pilotName;
        this.aerialVehicle = aerialVehicle;
    }

    public void begin() {
        System.out.println("Beginning Mission!");
        try {
            aerialVehicle.flyTo(destination);
        } catch (UnableToFlightException e) {
            e.printStackTrace();
        }
    }

    public void cancel() {
        System.out.println("Abort Mission!");
        aerialVehicle.land(aerialVehicle.getHomeBase());
    }

    public void finish() {
        try {
            System.out.println(this.executeMission());
        } catch (AerialVehicleNotCompatibleException e) {
            System.out.println(e.getMessage());
            this.cancel();
        }
        aerialVehicle.land(aerialVehicle.getHomeBase());
        System.out.println("Finish Mission!");
    }

    abstract public String executeMission() throws AerialVehicleNotCompatibleException;
}
