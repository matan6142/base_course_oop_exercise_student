package Missions;

import AerialVehicles.AerialVehicle;
import AerialVehicles.VehicleTypes.AerialBdaVehicle;
import Entities.Coordinates;

public class BdaMission extends Mission{
    private String objective;

    public BdaMission(Coordinates destination, String pilotName, AerialVehicle aerialVehicle, String objective) {
        super(destination, pilotName, aerialVehicle);
        this.objective = objective;
    }

    @Override
    public String executeMission() throws AerialVehicleNotCompatibleException {
        try {
            return String.format("%s: %s Taking pictures of suspect %s with: %s camera",
                                this.pilotName,
                                this.aerialVehicle.getClass().getSimpleName(),
                                objective,
                                ((AerialBdaVehicle)this.aerialVehicle).getCameraType().getValue());
        } catch (ClassCastException e) {
            throw new AerialVehicleNotCompatibleException(String.format("%s Types are unable to do %s",
                                                            this.aerialVehicle.getClass().getSimpleName(),
                                                            this.getClass().getSimpleName()));
        }
    }
}
