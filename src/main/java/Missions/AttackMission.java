package Missions;

import AerialVehicles.AerialVehicle;
import AerialVehicles.VehicleTypes.AerialAttackVehicle;
import Entities.Coordinates;

public class AttackMission extends Mission{
    private String target;

    public AttackMission(Coordinates destination, String pilotName, AerialVehicle aerialVehicle, String target) {
        super(destination, pilotName, aerialVehicle);
        this.target = target;
    }

    @Override
    public String executeMission() throws AerialVehicleNotCompatibleException {
        try {
            return String.format("%s: %s Attacking suspect %s with: %sX%d",
                    this.pilotName,
                    this.aerialVehicle.getClass().getSimpleName(),
                    target,
                    ((AerialAttackVehicle)this.aerialVehicle).getMissileType().getValue(),
                    ((AerialAttackVehicle)this.aerialVehicle).getMissileAmount());
        } catch (ClassCastException e) {
            throw new AerialVehicleNotCompatibleException(String.format("%s Types are unable to do %s",
                                                        this.aerialVehicle.getClass().getSimpleName(),
                                                        this.getClass().getSimpleName()));
        }
    }
}
