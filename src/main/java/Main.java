import AerialVehicles.AdditionTypes.CameraTypes;
import AerialVehicles.AdditionTypes.MissileTypes;
import AerialVehicles.AdditionTypes.SensorTypes;
import AerialVehicles.Drones.Hermes.Zik;
import AerialVehicles.FighterJets.F15;
import AerialVehicles.FighterJets.F16;
import Entities.Coordinates;
import Missions.AttackMission;
import Missions.BdaMission;
import Missions.IntelligenceMission;
import Missions.Mission;

public class Main {
    public static void main(String[] args) {
        Coordinates homeBase = new Coordinates(11.2,13.5);

        Mission attackMission = new AttackMission(new Coordinates(-0.118092,51.509865), "Perry the Platypus", new F15(homeBase ,5, MissileTypes.PYTHON, SensorTypes.INFRA_RED),"Dr Doofenshmirtz Building");
        attackMission.begin();
        attackMission.finish();

        // We expect to Abort the mission because the type of plan cant do this type of mission
        Mission intelligenceMission = new IntelligenceMission(new Coordinates(-4.152056,23.543901), "Candace", new F16(homeBase ,5, MissileTypes.PYTHON, CameraTypes.NIGHT_VISION),"Phineas and Ferb Invention");
        intelligenceMission.begin();
        intelligenceMission.finish();

        Mission bdaMission = new BdaMission(new Coordinates(6.774536,-0.441327), "Milo", new Zik(homeBase ,CameraTypes.NIGHT_VISION, SensorTypes.INFRA_RED),"Pistachios home base");
        bdaMission.begin();
        bdaMission.finish();
    }
}
